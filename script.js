const courses = [
	{
		id: "MEC01",
		name: "Static of Rigid Bodies",
		description: "First of the 4 Mechanics Series",
		price: 2000,
		isActive: true
	},
	{
		id: "CHM00A",
		name: "General Chemistry",
		description: "Introduction to Inorganic Chemistry",
		price: 1000,
		isActive: true
	},
	{
		id: "PE01",
		name: "Physical Fitness",
		description: "Introduction to Physical Education",
		price: 2000,
		isActive: true
	},
	{
		id: "BEC01",
		name: "Engineering Economy",
		description: "Application of Economy in Engineering",
		price: 4000,
		isActive: true
	}
]

const addCourse = (id, name, description, price, isActive) => {
	courses.push({
		id: id,
		name: name,
		description: description,
		price: price,
		isActive: isActive
	})
	alert(`You have created ${name}. Its price is ${price}.`)
}

const getSingleCourse = id => {
	const courseNeeded = courses.find(course => course.id === id);
	console.log(courseNeeded);
	return courseNeeded
}

const getAllCourse = () => {
	console.log(courses);
	return courses;
}

const archiveCourse = (index) => {
	console.log(index)
	if(courses[index].isActive) courses[index].isActive = false;
	console.log(courses[index])
}

const deleteCourse = () => {courses.pop()};

// Stretch Goals
const getActive = () => {
	const activeCourses = courses.filter(course => course.isActive)
	console.log(activeCourses);
	return activeCourses;
}

const archiveCourse2 = (name) => {
	const index = courses.findIndex(course => course.name === name);
	console.log(index)
	if(courses[index].isActive) courses[index].isActive = false;
	console.log(courses[index])
}

/*console.log(courses);
addCourse("ENG00", "Grammar", "English", 1000, true);
// getSingleCourse("ENG00");
// getAllCourse();
archiveCourse(0);
// deleteCourse();
showActiveCourses();
// archiveCourse2("Physical Fitness");*/